<?php 
class Query {
    protected $dbconnect;
    function __construct($dbconnect){
        $this->dbconnect = $dbconnect;

    }
    public function query($query){
        if($result = $this->dbconnect->query($query)){
            return $result;


        } else{
            echo "Something went wrong with the query";

        }
    }
}
?>